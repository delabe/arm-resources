#!/bin/bash
# $1 = Azure storage account name
# $2 = Azure storage account key
# $3 = Azure file share name
# $4 = mountpoint path
# $5 - username

# For more details refer to https://azure.microsoft.com/en-us/documentation/articles/storage-how-to-use-files-linux/

# update package lists
apt-get -y update

# Create a credential file to store the username (the storage account name) and password (the storage account key) for the file share
if [ ! -d "/etc/smbcredentials" ]; then
    mkdir /etc/smbcredentials
fi

if [ ! -f "/etc/smbcredentials/<storage-account-name>.cred" ]; then
    echo "username=$1" >> /etc/smbcredentials/$1.cred
    echo "password=$2" >> /etc/smbcredentials/$1.cred
fi

chmod 600 /etc/smbcredentials/$1.cred

# install cifs-utils and mount file share
apt-get install cifs-utils
if [ ! -d "$4" ]; then
    mkdir $4
fi

echo "//$1.file.core.windows.net/$3 $4 cifs nofail,vers=3.0,credentials=/etc/smbcredentials/$1.cred,dir_mode=0777,file_mode=0777,serverino" >> /etc/fstab

mount -a

# create marker files for testing
echo "hello from $HOSTNAME" > $4/$HOSTNAME.txt